Para disponibilizar o projeto publicamente, o time de infraestrutura criou uma
servidor no provedor de Cloud [Linode](https://www.linode.com/), com as seguintes 
especificações:

- 160 GB de disco
- 4 CPU
- 8 GB de RAM
- Sistema operacional Debian 12

Além disso, também foram criados três subdomínios para cada um dos componentes:

- Wagtail: [https://horizontes-empreendedor.site.pencillabs.tec.br/](https://horizontes-empreendedor.site.pencillabs.tec.br/)
- Moodle: [horizontes-empreendedor.moodle.pencillabs.tec.br/](horizontes-empreendedor.moodle.pencillabs.tec.br/)
- Keycloak: [horizontes-empreendedor.keycloak.pencillabs.tec.br/](horizontes-empreendedor.keycloak.pencillabs.tec.br/)
- Documentação: [horizontes-empreendedor.docs.pencillabs.tec.br/](horizontes-empreendedor.docs.pencillabs.tec.br/)

Dentro da servidor criado, configuramos uma instância do [Nginx](https://nginx.org/en/),
para atuar como *reverse proxy* dos containers de cada aplicação. O nginx foi instalado
diretamente no servidor via `apt`, enquanto as aplicaçãoes foram configuradas com os
seus respectivos `docker-compose.yml`. Para os certificados TLS, a equipe utilizou a
ferramenta [letsencrypt](https://letsencrypt.org/docs/). O Nginx fica responsável por
identificar o domínio da requisição e redirecionar para o respectivo 
container rodando dentro do servidor.

<figure>
    <img src="/images/infra/nginx.jpg" alt="">
    <figcaption><small>Organização dos componentes da infraestrutura</small></figcaption>
</figure>

!!! tip "Dica"

    No [repositório da infraestrutura](https://gitlab.com/pencillabs/plataformaempreenda/infraestrutura) 
    estão versionando os arquivos de configuração do Nginx, utilizados no ambiente de homologação da Pencillabs.
    
