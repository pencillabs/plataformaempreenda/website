| Variável                     | Valor padrão                                 | Descrição                                                                                                                         |
|------------------------------|----------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|
| KEYCLOAK_HOST                   | https://horizontes-empreendedor.keycloak.pencillabs.tec.br                                           | Domínio do gerenciador de identidades Keycloak.           |
| KEYCLOAK_DISCOVERY_ENDPOINT                  |  https://horizontes-empreendedor.keycloak.pencillabs.tec.br/realms/empreenda/.well-known/openid-configuration | Endpoint que permite com que aplicações descubram automaticamente as conigurações necessárias para se conectarem ao Keycloak.                                              |
| KEYCLOAK_TOKEN_ENDPOINT                  | https://horizontes-empreendedor.keycloak.pencillabs.tec.br/realms/empreenda/protocol/openid-connect/token | Endpoint responsável por emitir tokens de acesso JWT.                                               |
| KEYCLOAK_USERINFO_ENDPOINT                  | https://horizontes-empreendedor.keycloak.pencillabs.tec.br/realms/empreenda/protocol/openid-connect/userinfo | Endpoint utilizado para obter informações detalhadas sobre o usuário autenticado.   
| KEYCLOAK_AUTH_ENDPOINT                  | https://horizontes-empreendedor.keycloak.pencillabs.tec.br/realms/empreenda/protocol/openid-connect/auth | Endpoint responsável por requisitar um código de acesso, que depois será trocado por um token.     
| KEYCLOAK_REDIRECT_URI                  | https://horizontes-empreendedor.site.pencillabs.tec.br/accounts/oidc/keycloak/login/callback/ | Url do website que irá receber o token e realizará a autenticação do usuário no servidor do Django.  
| CLIENT_ID                  | wagtail-keycloak-id | Nome do cliente do website que foi criado no Keycloak. 
| CLIENT_SECRET                  | "***" | Senha gerada pelo Keycloak para permitir o acesso ao cliente wagtail-keycloak-id.  
| POSTGRES_USER                  | empreenda | Usuário que o Postgres irá utilizar para se conectar ao Wagtail.  
| POSTGRES_PASSWORD                  | empreenda | Senha que o Postgres irá utilizar para se conectar ao Wagtail.
| POSTGRES_DB                  | empreenda | Nome do banco de dados que será usado no Wagtail.  
| POSTGRES_PORT                  | 5432 | Porta da instância do Postgres. 
| POSTGRES_HOST                  | db | Host da instância do Postgres.
| EMAIL_HOST                  |  | 
| EMAIL_PORT                  |  |
| EMAIL_HOST_USER             |  | 
| EMAIL_HOST_PASSWORD                  |  |  
| EMAIL_USE_TLS                  |  |  
| RECAPTCHA_PUBLIC_KEY                  |  |  
| WAGTAILIMAGES_MAX_IMAGE_PIXELS                 |  |   
