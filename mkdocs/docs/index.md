O projeto Horizontes do Empreendedorismo é uma iniciativa da Universidade de Brasília 
em parceria com a empresa [Pencillabs](https://pencillabs.tec.br/), para desenvolver uma 
plataforma de cursos voltados para o tema do empreendedorismo. 
O projeto foi desenvolvido ao longo de 2024 por uma
equipe composta por bolsitas do campus Faculdade do Gama e desenvolvedores da empresa.

Esse site contém a documentação técnica do projeto, com instruções de como configurar
e disponibizar os componentes da arquitetura da aplicação, que são:

- **[Wagtail](https://wagtail.org/)**: Plataforma de gestão de conteúdo baseada no Django. É com o Wagtail que
é possível criar publicações, artigos, fazer upload de imagens e demais tarefas de
conteúdo relacionadas ao site institucional do projeto.
- **[Moodle](https://moodle.org/?lang=pt_br)**: Plataforma de aprendizado open-source. É no Moodle que ficam armazenados
os cursos do projeto.
- **[Keycloak](https://www.keycloak.org/)**: Solução open-source para gestão de identidade e acesso. É com o Keycloak
que garantimos que o usuário terá acesso ao Moodle e a recursos do site disponíveis
apenas para usuários autenticados.

Atualmente, a arquitetura do projeto está organizada da seguinte maneira:

<figure>
    <img src="/images/index/keycloak.png" alt="">
    <figcaption><small>Visão geral da arquitetura do projeto.</small></figcaption>
</figure>

Os repositórios para cada um dos componentes da arquitetura são os seguintes:

- [Site do projeto e da documentação técnica](https://gitlab.com/pencillabs/plataformaempreenda/website)
- [Keycloak](https://gitlab.com/pencillabs/plataformaempreenda/identity-provider)
- [Moodle](https://gitlab.com/pencillabs/plataformaempreenda/moodle)
- [Infraestrutura](https://gitlab.com/pencillabs/plataformaempreenda/infraestrutura)
