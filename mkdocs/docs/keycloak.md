
O [Keycloak](https://www.keycloak.org/) é uma solução open-source que permite centralizar o 
processo de identificação e autorização de usuários a partir do 
protocolo *Single Sign-On* (SSO). O Keycloak permite que o usuário se autentique
uma única vez para ter acesso à multiplas aplicações. No contexto do projeto 
Horizontes do Empreendedorismo, isso é utilizado para autenticar o usuário tanto no 
site quanto no Moodle utilizando um único formulário de registro.

!!! warning "Aviso importante"

    No ambiente local, tanto site quanto Moodle precisam utilizar o IP privado 
    da sua máquina para acessar o Keycloak. 
    Não utilize localhost nas variáveis de ambiente, mas sim
    o IP no formato **IP-PRIVADO-DA-SUA-MÁQUINA:PORTA-DO-KEYCLOAK**.

## Configuração

O fluxo de autenticação entre os provedores de serviço (Site e Moodle) e o Keycloak irá
funcionar da seguinte maneira:

1. O usuário não autenticado entra no website e é redirecionado para uma tela de Login/Registro do Keycloak.

2. Logo em seguida, a aplicação Cliente (Website-Wagtail) vai requisitar uma autorização para o Authorization Server (Keycloak), passando as credenciais fornecidas pelo usuário.

3. Se as credenciais forem válidas, um Authorization Code será enviado de volta para o Cliente.

4. O Cliente então fará outra requisição ao Authorization Server para trocar o Authorization Code por um Access Token. De posse desse token, é possível acessar recursos sobre o usuário no Authorization Server.

5. Depois de receber o Access Token, o Cliente vai fazer uma nova requisição para receber informações sobre o usuário que está tentando fazer a autenticação.

6. Com essas informações sobre o usuário, o Cliente vai registrar um novo usuário na plataforma, ou caso já exista, irá retorná-lo e, logo após, irá logar esse usuário.

<figure>
    <img src="/images/keycloak/AuthenticationFlow.png" alt="">
    <figcaption><small>Fluxo de autenticação com o Keycloak</small></figcaption>
</figure>

!!! info "Dica"

    Siga o README.md do repositório do Keycloak para subir o ambiente local.

Para permitir que o usuário se autentique uma única vez e tenha acesso a área logada
do site e ao Moodle, é preciso criar um *Realm* e um *Client* no painel do Keycloak.

### Realm

Um Realm permite gerenciar de forma isolada usuários, clientes, roles, 
políticas de segurança, etc. Portanto, é necessário criar um realm para o projeto, 
ao invés de utilizar o padrão do Keycloak. 
Existe um botão `Create realm`, na tela inicial do usuário logado.
Para cria-lo, basta definir um nome, por exemplo, "empreenda".
Após criar o Realm, acesse a página `Realm Settings -> Login` e 
habilite a opção `User registration`.

#### Recuperação de senha

Também será necessário configurar a opção de recuperação de senha. 
Acesse `Realm Settings -> Login ` e habilite a opção `Forgot password`.
Ainda na página de configurações do Realm, faça a configuração do servidor SMTP
que será utilizado para disparo dos emails.

<figure>
    <img src="/images/keycloak/smtp.png" alt="">
    <figcaption><small>Configuração do template para envio de emails</small></figcaption>
</figure>

Na seção "Connection & Authentication" será preciso informar as credenciais do servidor
SMTP. Por uma questão de segurança, não disponibilizaremos a senha de acesso na
documentação, mas ela pode ser solicitada para a coordenação do projeto. 
O Serviço SMTP utilizado pelo projeto é o [SendGrid](app.sendgrid.com).

<figure>
    <img src="/images/keycloak/sendgrid.png" alt="">
    <figcaption><small>Exemplo de configuração para autenticação no SendGrid.</small></figcaption>
</figure>


### Client
Para logar e registrar novos usuários usando o Keycloak é preciso adicionar um novo cliente no realm empreenda. Para configurar um novo cliente, acesse a opção **Clients** no menu lateral e clique em **Create client**.

!!! info "Dica"

    Cada aplicação que irá utilizar o Keycloak para autenticação precisa ter um
    client criado.


Acesse a página `Clients` e clique em `Create Client`. Utilize a seguinte configuração 
para o site:

**General Settings**

- Client type: OpenID Connect
- clientId: site-keycloak-id
- name: empreenda
- description: Cliente do Keycloak para o Wagtail.

**Capability Config**

- Client authentication: On
- Authentication Flow: "Standard Flow", "Direct Access Grants" e "Service Accounts Roles"
- Client Authentication: On

**Login/Access Settings**

- Valid Redirect URIs: http://localhost:4000/accounts/oidc/keycloak/login/callback
- Valid post logout redirect URISs: '+'
- Web Origins: http://localhost:4000

Após salvar o cliente, acesse a aba `Credentials` e selecione o `Client Authenticator` como  "Client ID and Secret". O Wagtail irá precisar do `Client ID` e do `Client Secret` 
para se autenticar no Keycloak. Atualize as variáveis de 
ambiente `CLIENT_ID` e `CLIENT_SECRET` com os valores corretos.

Depois faça o mesmo processo para cliar um cliente para a aplicação do [Moodle](https://gitlab.com/pencillabs/plataformaempreenda/moodle), com as seguintes variáveis:

* Client type: "OpenID Connect"
* clientId: "moodle-keycloak-id"
* name: "moodle"
* Valid redirect URIs: "http://localhost:8085/admin/oauth2callback.php"
* webOrigins: "http://localhost:8085"


**Endpoints usados durante esse processo**

* Para requisitar um Authorization Code: http://keycloak:8080/realms/empreenda/.well-known/openid-configuration

* Para requisitar um Access Token:
http://keycloak:8080/realms/empreenda/protocol/openid-connect/token

* Para requisitar informações do Usuário:
http://keycloak:8080/realms/empreenda/protocol/openid-connect/userinfo


## Tema customizado

A equipe do projeto desenvolveu um tema visual para o Keycloak que seguisse a identididade
visual do site. O tema pode ser selecionado na área de configuração do Realm.

<figure>
    <img src="/images/keycloak/login.png" alt="">
    <figcaption><small>Tela de login no Keycloak</small></figcaption>
</figure>

<figure>
    <img src="/images/keycloak/login-config.png" alt="">
    <figcaption><small>Tela para seleção do tema</small></figcaption>
</figure>

## Referências

- https://docs.allauth.org/en/latest/installation/quickstart.html
- https://docs.allauth.org/en/latest/installation/quickstart.html
- https://medium.com/@robertjosephk/setting-up-keycloak-in-django-with-django-allauth-cfc84fdbfee2
- https://princeigwe.medium.com/seamless-authentication-integrating-openid-connect-with-django-using-auth0-35d83068f197

