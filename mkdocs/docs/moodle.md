O [Moodle](https://moodle.org/?lang=pt_br) é uma plataforma de aprendizagem e gestão de cursos online. O projeto Horizontes do Empreendedorismo possui dois cursos 
voltados para o público de jovens negros e mulheres, que precisam ser importados
na instância do Moodle. Siga o [README.md](https://gitlab.com/pencillabs/plataformaempreenda/moodle) do repositório para a instalação inicial do projeto.

## Continuando a instalação

Com os containers do Moodle rodando, será preciso realizar algumas configurações para
termos o ambiente integrado ao Keycloak e com os cursos disponíveis.

### Location settings e Manage authentication

- Na tela de configuração do site, selecione o timezone padrão para `America/Sao_Paulo`.
- Na tela de configuração do site, desabilite a opção `Self registration`.

Prossiga para finalizar a instalação, o restante dos campos podem ser preenchidos livremente.

### Instale o plugin format_grid

Com o Moodle rodando no seu ambiente, será preciso instalar o plugin [`format_grid`](https://moodle.org/plugins/format_grid). Faça o download do 
arquivo `.zip` do diretório de plugins do Moodle. 
Em seguida, instale o plugin pela área de administração da plataforma.

!!! note "Dica"
    Baixe o plugin que seja compativel com a versão 4.3 do Moodle.

### Importe o backup dos cursos

Para restaurar o curso, acesse `More > My courses > Create course`. Na página de
criação, clique na opção `More` e selecione `Restore course`. O arquivo dos dois cursos
do projeto estão no diretório `courses` do repositório.

!!! warning "Aviso"

    Caso o usuário administrador do ambiente se chame "admin", 
    a importação dará problema, então é preciso desabilitar a opção "Include enrolled users".

Repita os mesmos procedimentos para ambos os cursos.

<figure>
    <img src="/images/moodle/curso_importado.png" alt="">
    <figcaption><small>Curso para mulheres jovens importado no Moodle</small></figcaption>
</figure>

### Colocando o Moodle em Português

Para que a plataforma esteja em português, precisamos adicionar um `language pack`.
Acesse `Site Administration -> General -> Language -> Language packs`. Nessa tela,
selecione e instale o pacote "Portuguese - Brazil". Em seguida,  
acesse `Site Administration -> General -> Language -> Language settings` e
altere a configuração `Default language` para Português.

!!! note "Dica"

    Caso o idioma do Moodle não mude automaticamente, é provavável que a configuração
    "Preferred language", na área de perfil do seu usuário esteja em inglês. Alterando
    para Português, o Moodle ficará no idioma correto.


## SMTP

| Variável | Descrição | valor |
|----------|-----------|-------|
| MOODLE_SMTP_HOSTS       | Lista de servidores SMTP utilizados para enviar emails           |       smtp.gmail.com |
| MOODLE_NOREPLY_ADDRESS       | Email de origem que irá aparecer para o usuário na sua caixa de entrada.           |       horizontesempreendedorismo@gmail.com |


!!! tip "Dica"

    O servidor SMTP utilizado pelo Moodle será o mesmo que configuramos para o Keycloak.
    Veja na seção de [recuperação de senha](/keycloak#recuperação-de-senha)


## Configuração da autenticação usando Keycloak
Para se registrar e logar usando o provedor de identidades Keycloak, realize os seguintes passos:


1. Crie um Cliente no Keycloak, dentro do realm Empreenda, seguindo as instruções da documentação da página do [Keycloak](/keycloak).

2. Acesse a seção de Administração do Moodle, estando logado como usuário admin. Logo após acesse `Plugins > Authentication > Manage authentication` e clique no enable na opção OAuth2.

![Ativar o Oauth2](static/img/enable-oauth2.png)

3. Configure o provider do OAuth2/OpenID Connect. Para isso, acesse `Site administration >> Server > Server OAuth2 services > Create new service > Custom` e altere os campos Name, Client ID, Client Secret e Service Base URL para ficar com as seguintes configurações:

- **Name:** Keycloak
- **Client ID:** moodle-keycloak-id
- **Client Secret:** `colocar aqui a credencial do cliente que foi criada no Keycloak`
- **Service Base URL:** http://localhost:8080
- **This service will be used**: Login page and internal services
- **Additional parameters included in a login request.**: redirect_uri=http://localhost:8085/admin/oauth2callback.php
 
 ![Configuração do Provider](static/img/keycloak-provider.png)

 4. Configure os endpoints do Keycloak. Acesse `Site administration > Server OAuth2 services` e na opção **Edit**, clique no **Configure endpoints**. Os seguintes endpoints precisam ser configurados: discovery_endpoint, authorization_endpoint, token_endpoint e userinfo_endpoint, conforme a figura abaixo.

  ![Configuração dos Endpoints](static/img/keycloak-endpoints.png)

5. Mapeei os campos de registro entre o Keycloak e o Moodle. Para isso, Acesse `Site administration >> Server OAuth2 services` e na opção **Edit**, clique no **Configure user field mappings**. Configure os campos `email, given_name, family_name e preferred_name`, conforme a figura abaixo.

<figure>
    <img src="/images/moodle/user-field-mapping.png" alt="">
    <figcaption><small>Mapeando campos do Moodle para o serviço Oauth2</small></figcaption>
</figure>
