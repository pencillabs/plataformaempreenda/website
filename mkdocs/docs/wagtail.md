O [Wagtail](https://wagtail.org/) é um gerenciador de conteúdos feito em Python e
baseado no Framework Django. Com ele, é possível desenvolver plataformas que tenham 
em seus requisitos a necessidade de produção de páginas com conteúdos dinâmicos. Por
ser baseado no Django, permite que os desenvolvedores customizem todo o backend e
a estrutura do banco de dados, além de poder utilizar todos os pacotes Python disponíveis
em repositórios como [https://pypi.org/](https://pypi.org/).


## Gestão de conteúdos

No contexto do Horizontes do Empreendedorismo, o Wagtail foi utilizado para implementarmos
a página inicial e a área de Blog do site. A maioria dos conteúdos visiveis nessas duas
áreas são editáveis pelo painel administrativo do Wagtail. 

!!! tip "Dica"

    Para acessar a área de gestão
    de conteúdos, é preciso criar um usuário administrador via linha de comando, dentro
    do container Docker do site. Acesse o README.md do repositório para mais instruções.

<figure>
    <img src="/images/wagtail/admin1.png" alt="">
    <figcaption><small>Painel de gestão de conteúdos do Wagtail</small></figcaption>
</figure>

A produção de conteúdos para o blog também é feita pelo painel administrativo. O Wagtail
possui uma funcionalidade de moderação, que pode ser utilizada para criar um fluxo de
revisão dos conteúdos produzidos.

<figure>
    <img src="/images/wagtail/admin2.png" alt="">
    <figcaption><small>Exemplo de publicação do blog do projeto.</small></figcaption>
</figure>

<figure>
    <img src="/images/wagtail/lp.png" alt="">
    <figcaption><small>Lista de publicações vinculadas na página inicial</small></figcaption>
</figure>

## Autenticação

O site é o ponto de partida do usuário. Todos os conteúdos sobre os cursos e sobre o tema 
de empreendedorismo estarão disponíveis no blog e na página inicial. O site também
centraliza o fluxo de autenticação, para que o usuário tenha acesso aos cursos disponíveis
no Moodle.

Todo o processo de registro, autenticação e recuperação de senha precisa ser feita pelo
Keycloak, para garantirmos o fluxo transparente de autenticação entre os sistemas. Para
configurar um *client* do Wagtail para o Keycloak, acesse a nossa 
[documentação de autenticação](/keycloak/#client).
