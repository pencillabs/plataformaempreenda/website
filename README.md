# Website

Repositório contendo o site institucional do projeto Empreenda UnB.
O site é desenvolvido utilizando o framework CMS [Wagtail](https://wagtail.org/).

# Quick Start

**1. Clone o repositório**

    $ git clone git@gitlab.com:pencillabs/plataformaempreenda/website.git
    $ cd website

**2. Instale [o plugin compose do Docker](https://docs.docker.com/compose/install/linux/#install-using-the-repository)**

**3. Crie um arquivo .env a partir do arquivo .sample.env**

    cp .sample.env .env

Revise as variáveis de ambiente do arquivo .env e verifique se algo precisa ser alterado
para o seu ambiente local.

**Exemplo de configuração**

```
KEYCLOAK_HOST=http://192.168.15.133:8080
WEBSITE_HOST=http://localhost:4000
KEYCLOAK_DISCOVERY_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/.well-known/openid-configuration
KEYCLOAK_TOKEN_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/protocol/openid-connect/token
KEYCLOAK_USERINFO_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/protocol/openid-connect/userinfo
KEYCLOAK_AUTH_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/protocol/openid-connect/auth
KEYCLOAK_LOGOUT_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/protocol/openid-connect/logout
KEYCLOAK_REGISTER_ENDPOINT=${KEYCLOAK_HOST}/realms/empreenda/login-actions/registration
KEYCLOAK_REDIRECT_URI=${WEBSITE_HOST}/accounts/oidc/keycloak/login/callback

CLIENT_ID=site-keycloak-id
CLIENT_SECRET=hSJdLMbcujROEEkZOiEsTOMBZh8qnY4Y

POSTGRES_USER=empreenda
POSTGRES_PASSWORD=empreenda
POSTGRES_DB=empreenda
POSTGRES_PORT=5432
POSTGRES_HOST=db

EMAIL_HOST=
EMAIL_PORT=587 
EMAIL_HOST_USER=
EMAIL_HOST_PASSWORD=
EMAIL_USE_TLS=True

RECAPTCHA_PUBLIC_KEY=
RECAPTCHA_PRIVATE_KEY=

WAGTAILIMAGES_MAX_IMAGE_PIXELS=1280000000000
DJANGO_ALLOWED_HOSTS=localhost,0.0.0.0
DJANGO_CSRF_TRUSTED_ORIGINS=
```

**3. Crie os containers do site**

    $ docker compose build
    $ docker compose up

O site estará disponível na URL [http://localhost:4000/](http://localhost:4000/).

Para acessar o painel administrativo do Wagtail, acesse o container do servidor:

    docker exec -it empreenda_server bash

E crie um usuário administrador:

    cd empreenda
    ./manage.py createsuperuser

Com as credenciais do usuário administrador, acesse o painel do Wagtail em 
[http://localhost:4000/admin](http://localhost:4000/admin).

# Documentação

O projeto utiliza a biblioteca `mkdocs` para versionar a documentação de arquitetura e
detalhes de configuração. Para gerar a documentação completa, execute o comando:

    docker compose -f mkdocs/docker-compose.yml up

A documentação estará disponível na URL [http://localhost:8001]

## Tailwind + Flowbite

Este projeto utiliza o [Flowbite](https://flowbite.com/), uma biblioteca de componentes de UI construída sobre o Tailwind CSS. 

Para instalar o [Flowbite](https://flowbite.com/) é necessário instalar os pacotes do package.json por meio do comando `npm install`, dentro do container do Empreenda.

Observação: Caso o npm não esteja instalado dentro do seu container, execute o comando `docker compose build --no-cache`.

## Temas no CSS
É possível criar variáveis para serem reutilizadas no html e no css, por exemplo, variáveis de cores. Para isso altere o arquivo `tailwind.config.js` e, logo após, compile essas mudanças usando o comando `npm run build`.
