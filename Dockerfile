FROM python:3.11.9-slim-bookworm
RUN apt update && apt install -y curl libpq-dev gcc nodejs npm
ENV PATH="${PATH}:/root/.local/bin"
RUN curl -sSL https://install.python-poetry.org | python3 -
WORKDIR /code/
COPY . .
RUN poetry config virtualenvs.create false
RUN poetry install
