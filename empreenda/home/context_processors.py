from home.models import MenuItem, PrivacyPolicyPage, FooterSection
from django.utils import timezone


def get_current_date_with_words(current_date):
    months_map = {
        "January": "Janeiro",
        "February": "Fevereiro",
        "March": "Março",
        "April": "Abril",
        "May": "Maio",
        "June": "Junho",
        "July": "Julho",
        "August": "Agosto",
        "September": "Setembro",
        "October": "Outubro",
        "November": "Novembro",
        "December": "Dezembro",
    }

    day = current_date.day
    month = current_date.strftime("%B")

    month_in_portuguese = months_map[month]
    return f"{day} de {month_in_portuguese}"


def global_context(request):
    current_date = timezone.now()
    return {
        "menu_items": MenuItem.objects.order_by("sort_order"),
        "privacy_policy_page": PrivacyPolicyPage.objects.live().first(),
        "footer_section": FooterSection.objects.order_by("-id").first(),
        "current_date": current_date.strftime("%d/%m/%Y %H:%M"),
        "current_date_with_words": get_current_date_with_words(current_date),
        "path_segments": request.path.strip("/").split("/"),
    }
