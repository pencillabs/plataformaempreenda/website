from django.db import models
from wagtail.models import Page
from wagtail.admin.panels import FieldPanel


class FaqSection(Page):
    background_image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    description = models.CharField(max_length=255)
    faq_url = models.CharField(max_length=255)
    blog_url = models.CharField(max_length=255)

    content_panels = Page.content_panels + [
        FieldPanel("background_image"),
        FieldPanel("description"),
        FieldPanel("faq_url"),
        FieldPanel("blog_url"),
    ]
