from .base import HomePage
from .info_section import InfoCard, InfoSection
from .carousel_section import CarouselSection
from .footer_section import FooterSection
from .banner_section import BannerSection
from .menu import MenuItem
from .privacy_policies import PrivacyPolicyPage
from .inscricao_section import InscricaoSection
