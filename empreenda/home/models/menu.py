from django.db import models
from wagtail.admin.panels import FieldPanel, InlinePanel, PageChooserPanel
from modelcluster.fields import ParentalKey
from wagtail.models import Orderable
from modelcluster.models import ClusterableModel
from django.core.exceptions import ValidationError
from wagtail.snippets.models import register_snippet


@register_snippet
class Menu(ClusterableModel):
    title = models.CharField(max_length=50)

    panels = [FieldPanel("title"), InlinePanel("menu_items", label="Menu Items")]

    def __str__(self):
        return self.title


class MenuItem(Orderable):
    link_title = models.CharField(
        blank=True,
        null=True,
        max_length=50,
        help_text="Optional title for the menu item.",
    )
    link_url = models.CharField(
        max_length=500, blank=True, help_text="External URL for the menu item."
    )
    link_page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.CASCADE,
        help_text="Select an internal page to link to.",
    )
    open_in_new_tab = models.BooleanField(
        default=False, blank=True, help_text="Open link in a new tab."
    )

    menu = ParentalKey(
        "Menu",
        related_name="menu_items",
        default=None,
        on_delete=models.SET_NULL,
        null=True,
    )

    panels = [
        FieldPanel("link_title"),
        FieldPanel("link_url"),
        PageChooserPanel("link_page"),
        FieldPanel("open_in_new_tab"),
    ]

    def clean(self):
        if not self.link_url and not self.link_page:
            raise ValidationError("You must specify either a URL or an internal page.")

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_url:
            return self.link_url
        return "#"

    @property
    def title(self):
        if self.link_page and not self.link_title:
            return self.link_page.title
        elif self.link_title:
            return self.link_title
        return "Untitled Menu Item"
