from wagtail.admin.panels import FieldPanel, InlinePanel, PageChooserPanel
from wagtail.snippets.models import register_snippet
from wagtail.models import Orderable, ClusterableModel
from modelcluster.fields import ParentalKey
from django.core.exceptions import ValidationError
from django.db import models


@register_snippet
class FooterSection(ClusterableModel):
    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    email = models.EmailField(
        blank=True,
        null=True,
        help_text="Email exibido no rodapé.",
    )
    address = models.CharField(
        max_length=255,
        blank=True,
        null=True,
        help_text="Endereço exibido no rodapé.",
    )

    panels = [
        FieldPanel("logo"),
        FieldPanel("email"),
        FieldPanel("address"),
        InlinePanel("parceiros", label="Parceiros"),
        InlinePanel("footer_links", label="Footer Links"),
    ]

    def __str__(self):
        return self.email if self.email else f"FooterSection ({self.pk})"


@register_snippet
class Parceiro(Orderable):
    footer_section = ParentalKey(
        "FooterSection", related_name="parceiros", on_delete=models.CASCADE
    )
    logo = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    link = models.URLField(
        max_length=500,
        blank=True,
        null=True,
        help_text="URL do parceiro, será usado como link ao clicar no logo.",
    )

    panels = [
        FieldPanel("logo"),
        FieldPanel("link"),
    ]


class FooterLink(Orderable):
    link_title = models.CharField(
        blank=True,
        null=True,
        max_length=100,
        help_text="Título opcional do link (máximo de 100 caracteres).",
    )
    link_url = models.CharField(
        max_length=500,
        blank=True,
        help_text="URL externa para o link do rodapé.",
    )
    link_page = models.ForeignKey(
        "wagtailcore.Page",
        null=True,
        blank=True,
        related_name="+",
        on_delete=models.CASCADE,
        help_text="Selecione uma página interna para vincular.",
    )
    open_in_new_tab = models.BooleanField(
        default=False,
        blank=True,
        help_text="Abrir o link em uma nova aba.",
    )

    footer_section = ParentalKey(
        "FooterSection", related_name="footer_links", on_delete=models.CASCADE
    )

    panels = [
        FieldPanel("link_title"),
        FieldPanel("link_url"),
        PageChooserPanel("link_page"),
        FieldPanel("open_in_new_tab"),
    ]

    def clean(self):
        if not self.link_url and not self.link_page:
            raise ValidationError(
                "Você deve especificar uma URL ou uma página interna."
            )

    @property
    def link(self):
        if self.link_page:
            return self.link_page.url
        elif self.link_url:
            return self.link_url
        return "#"

    @property
    def title(self):
        if self.link_page and not self.link_title:
            return self.link_page.title
        elif self.link_title:
            return self.link_title
        return "Link sem título"
