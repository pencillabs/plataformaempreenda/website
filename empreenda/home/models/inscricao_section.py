from django.db import models
from wagtail.models import Page
from wagtail.admin.panels import FieldPanel


class InscricaoSection(Page):

    parent_page_types = ["HomePage"]

    inscricao_title = models.CharField(
        max_length=250,
        help_text="Adicione um título para a seção.",
    )

    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Adicione uma imagem para a seção.",
    )

    content_panels = Page.content_panels + [
        FieldPanel("inscricao_title"),
        FieldPanel("image"),
    ]
