from django.db import models
from wagtail.models import Page, Orderable
from wagtail.admin.panels import FieldPanel, InlinePanel
from modelcluster.fields import ParentalKey


class InfoCard(Orderable):
    page = ParentalKey(
        "InfoSection", related_name="info_cards", on_delete=models.CASCADE
    )
    card_short_title = models.CharField(max_length=255, null=True, blank=True)
    card_short_description = models.CharField(max_length=255, null=True, blank=True)
    card_title = models.CharField(max_length=255, null=True, blank=True)
    card_description = models.CharField(max_length=1024, null=True, blank=True)
    card_link_botao_saiba_mais = models.URLField(
        null=True, blank=True, help_text="URL do link associado ao cartão"
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )

    panels = [
        FieldPanel("card_short_title"),
        FieldPanel("card_short_description"),
        FieldPanel("card_title"),
        FieldPanel("card_description"),
        FieldPanel("card_link_botao_saiba_mais"),
        FieldPanel("image"),
    ]


class InfoSection(Page):
    content_panels = Page.content_panels + [
        InlinePanel("info_cards", label="Cards"),
    ]

    parent_page_types = ["HomePage"]
