from django.db import models
from wagtail.models import Page
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.snippets.models import register_snippet
from modelcluster.fields import ParentalKey
from wagtail.models import Orderable


class CarouselSection(Page):
    content_panels = Page.content_panels + [
        InlinePanel("carousel_slides", label="Slides"),
    ]

    parent_page_types = ["HomePage"]


@register_snippet
class CarouselSlide(Orderable):
    page = ParentalKey(
        "CarouselSection", related_name="carousel_slides", on_delete=models.CASCADE
    )
    title = models.CharField(max_length=150, null=True, blank=True)
    description = models.CharField(max_length=180, null=True, blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    more_info_redirect_uri = models.CharField(max_length=255, null=True, blank=True)

    panels = [
        FieldPanel("title"),
        FieldPanel("description"),
        FieldPanel("image"),
        FieldPanel("more_info_redirect_uri"),
    ]
