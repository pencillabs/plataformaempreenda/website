import os
from wagtail.models import Page
from django.utils.http import urlencode

from home.models.parceiros_section import ParceirosSection
from .info_section import InfoSection
from .carousel_section import CarouselSection
from .faq_section import FaqSection
from .footer_section import FooterSection
from .banner_section import BannerSection
from .privacy_policies import PrivacyPolicyPage
from .menu import MenuItem
from blog.models import BlogPost
from blog.models import BlogIndexPage
from .inscricao_section import InscricaoSection


class HomePage(Page):
    def get_context(self, request):
        context = super().get_context(request)

        # autenticação
        context["is_logged_in"] = request.user.is_authenticated

        context["info_section"] = InfoSection.objects.child_of(self).live()
        context["carousel_section"] = CarouselSection.objects.child_of(self).live()
        context["faq_section"] = FaqSection.objects.child_of(self).live()
        context["banner_section"] = BannerSection.objects.child_of(self).live()
        latest_posts = BlogPost.objects.live().order_by("-first_published_at")[:4]
        context["parceiros_section"] = ParceirosSection.objects.child_of(self).live()
        context["latest_posts"] = latest_posts
        context["footer_section"] = FooterSection.objects.order_by("-id").first()
        context["blog_index_page"] = BlogIndexPage.objects.live().first()
        context["menu_items"] = MenuItem.objects.order_by("id")
        context["login_url"] = self.get_login_url()
        context["register_url"] = self.get_register_url()
        context["privacy_policy_page"] = PrivacyPolicyPage.objects.live().first()
        context["edit_account_url"] = self.get_edit_account_url()
        context["inscricao_section"] = (
            InscricaoSection.objects.child_of(self).live().first()
        )
        return context

    def get_params(self):
        KEYCLOAK_REDIRECT_URI = os.getenv("KEYCLOAK_REDIRECT_URI")
        CLIENT_ID = os.getenv("CLIENT_ID")
        scope = "email profile openid"
        response_type = "code"

        return urlencode(
            {
                "client_id": CLIENT_ID,
                "redirect_uri": KEYCLOAK_REDIRECT_URI,
                "scope": scope,
                "response_type": response_type,
            }
        )

    def get_login_url(self):
        KEYCLOAK_AUTH_ENDPOINT = os.getenv("KEYCLOAK_AUTH_ENDPOINT")
        return f"{KEYCLOAK_AUTH_ENDPOINT}?{self.get_params()}"

    def get_register_url(self):
        KEYCLOAK_REGISTER_ENDPOINT = os.getenv("KEYCLOAK_REGISTER_ENDPOINT")
        return f"{KEYCLOAK_REGISTER_ENDPOINT}?{self.get_params()}"

    def get_edit_account_url(self):
        KEYCLOAK_EDIT_ACCOUNT_ENDPOINT = os.getenv("KEYCLOAK_EDIT_ACCOUNT_ENDPOINT")
        return f"{KEYCLOAK_EDIT_ACCOUNT_ENDPOINT}?{self.get_params()}"
