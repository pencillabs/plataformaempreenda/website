from django.db import models
from wagtail.models import Page
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.snippets.models import register_snippet
from modelcluster.fields import ParentalKey
from wagtail.models import Orderable


class ParceirosSection(Page):
    content_panels = Page.content_panels + [
        InlinePanel("parceiros_logo", label="logo"),
    ]

    parent_page_types = ["HomePage"]


@register_snippet
class ParceirosLogo(Orderable):
    page = ParentalKey(
        "ParceirosSection", related_name="parceiros_logo", on_delete=models.CASCADE
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )

    url = models.URLField(blank=True, null=True)

    panels = [
        FieldPanel("image"),
        FieldPanel("url"),
    ]
