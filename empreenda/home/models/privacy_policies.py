from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel


class PrivacyPolicyPage(Page):
    body = RichTextField(
        features=["bold", "italic", "underline", "h2", "h3", "h4", "ol", "ul", "link"]
    )

    content_panels = Page.content_panels + [
        FieldPanel("body"),
    ]
