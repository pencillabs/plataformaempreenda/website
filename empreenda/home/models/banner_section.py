from django.db import models
from wagtail.models import Page
from wagtail.admin.panels import FieldPanel
from blog.models import BlogPost


class BannerSection(Page):
    tag = models.CharField(max_length=255, default="event")
    parent_page_types = ["HomePage"]

    content_panels = Page.content_panels + [
        FieldPanel("tag"),
    ]

    @property
    def get_most_recent_tag(self):
        """
        Busca um post com a data mais próximo da data atual, a partir de uma tag escolhida no painel admin.
        """
        posts = BlogPost.objects.filter(tags__name__in=[self.tag])
        most_recent = posts.filter().order_by("-id").first()
        return most_recent
