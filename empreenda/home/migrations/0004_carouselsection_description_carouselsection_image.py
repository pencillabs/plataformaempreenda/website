# Generated by Django 5.0.6 on 2024-07-24 14:13

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0003_blogsection_carouselsection_contactsection_and_more"),
        ("wagtailimages", "0026_delete_uploadedimage"),
    ]

    operations = [
        migrations.AddField(
            model_name="carouselsection",
            name="description",
            field=models.CharField(blank=True, max_length=180, null=True),
        ),
        migrations.AddField(
            model_name="carouselsection",
            name="image",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="+",
                to="wagtailimages.image",
            ),
        ),
    ]
