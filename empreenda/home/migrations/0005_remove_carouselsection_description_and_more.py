# Generated by Django 5.0.6 on 2024-07-24 14:26

import django.db.models.deletion
import modelcluster.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("home", "0004_carouselsection_description_carouselsection_image"),
        ("wagtailimages", "0026_delete_uploadedimage"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="carouselsection",
            name="description",
        ),
        migrations.RemoveField(
            model_name="carouselsection",
            name="image",
        ),
        migrations.CreateModel(
            name="CarouselSlide",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "sort_order",
                    models.IntegerField(blank=True, editable=False, null=True),
                ),
                (
                    "description",
                    models.CharField(blank=True, max_length=180, null=True),
                ),
                (
                    "image",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        related_name="+",
                        to="wagtailimages.image",
                    ),
                ),
                (
                    "page",
                    modelcluster.fields.ParentalKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="carousel_slides",
                        to="home.carouselsection",
                    ),
                ),
            ],
            options={
                "ordering": ["sort_order"],
                "abstract": False,
            },
        ),
    ]
