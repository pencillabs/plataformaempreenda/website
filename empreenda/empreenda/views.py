from django.http import JsonResponse, HttpResponseBadRequest, HttpResponseNotAllowed
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import login
from django.utils.decorators import method_decorator
from django.shortcuts import redirect
from .models import KeycloakClient
from django.contrib.auth import get_user_model
from django.views import View


class KeycloakCallbackView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        User = get_user_model()

        code = request.GET.get("code")
        if not code:
            return JsonResponse(
                {"error": "Authorization code not provided"}, status=400
            )

        client = KeycloakClient(code)
        access_token = client.get_token()

        if access_token:
            user_data = client.get_userinfo(access_token)
            user = User.get_or_create_user(user_data)
            login(request, user)
            return redirect("/")

        return HttpResponseBadRequest("Access token not found in the response")

    def post(self, request):
        return HttpResponseNotAllowed("Invalid request method")
