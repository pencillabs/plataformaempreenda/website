import os
import requests
from django.shortcuts import redirect
from django.http import HttpResponseBadRequest, HttpResponse


class KeycloakClient:
    TOKEN_ENDPOINT = os.getenv("KEYCLOAK_TOKEN_ENDPOINT")
    USERINFO_ENDPOINT = os.getenv("KEYCLOAK_USERINFO_ENDPOINT")

    def __init__(self, auth_code):
        self.auth_code = auth_code

    def get_request_data(self):
        return {
            "grant_type": "authorization_code",
            "code": self.auth_code,
            "redirect_uri": os.getenv("KEYCLOAK_REDIRECT_URI"),
            "client_id": os.getenv("CLIENT_ID"),
            "client_secret": os.getenv("CLIENT_SECRET"),
        }

    def get_token(self):
        try:
            response = requests.post(self.TOKEN_ENDPOINT, data=self.get_request_data())
        except Exception:
            return HttpResponseBadRequest(response.json())

        if response.status_code == 200:
            token_data = response.json()
            return token_data.get("access_token")

        return HttpResponse(
            "Failed to retrieve access token", status=response.status_code
        )

    def get_userinfo(self, access_token):
        headers = {"Authorization": f"Bearer {access_token}"}
        response = requests.get(self.USERINFO_ENDPOINT, headers=headers)

        if response.status_code == 200:
            return response.json()

        return redirect("/account/login/")
