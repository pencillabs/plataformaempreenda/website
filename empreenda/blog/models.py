from django.db import models
from wagtail.models import Page
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel, InlinePanel
from wagtail.search import index
from wagtail.models import Orderable
from modelcluster.fields import ParentalKey
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from home.models.inscricao_section import InscricaoSection


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]

    def get_context(self, request):
        context = super().get_context(request)

        # Seção de inscrição
        inscricao_section = InscricaoSection.objects.live().first()
        context["inscricao_section"] = inscricao_section

        # posts mais recentes
        blogpost = (
            BlogPost.objects.live().descendant_of(self).order_by("-first_published_at")
        )

        # Definir a primeira seção com os 3 primeiros posts
        featured_posts = blogpost[:3]
        context["featured_posts"] = featured_posts

        # Exibir todos os posts para seção de paginação
        all_posts = blogpost

        # Filtragem por busca e tags
        search_query = request.GET.get("search")
        if search_query:
            all_posts = all_posts.filter(title__icontains=search_query)

        tag = request.GET.get("tag")
        if tag:
            all_posts = all_posts.filter(tags__slug=tag)

        # Filtra apenas posts marcados para aparecer nos cards principais
        featured_posts = (
            BlogPost.objects.live()
            .descendant_of(self)
            .filter(show_in_featured=True)
            .order_by("-first_published_at")[:3]
        )
        context["featured_posts"] = featured_posts

        # Paginação
        paginator = Paginator(all_posts, 4)  # Mostra 4 posts por página
        page = request.GET.get("page")
        try:
            paginated_posts = paginator.page(page)
        except PageNotAnInteger:
            paginated_posts = paginator.page(1)
        except EmptyPage:
            paginated_posts = paginator.page(paginator.num_pages)

        context["paginated_posts"] = paginated_posts

        # Lista de tags para filtragem
        tags = list(set(BlogPost.objects.live().values_list("tags__name", flat=True)))
        context["tags"] = tags

        # Carousel section
        carousel_blog_section = (
            CarouselBlogSection.objects.child_of(self).live().first()
        )
        if carousel_blog_section:
            context["carousel_blog_slides"] = (
                carousel_blog_section.carousel_blog_slides.all()
            )
        else:
            context["carousel_blog_slides"] = []

        # Link para "saiba mais" usando uma tag específica
        saiba_mais = BlogPost.objects.filter(tags__name="saiba_mais").live().first()
        if saiba_mais:
            context["saiba_mais"] = saiba_mais.get_url()
        else:
            context["saiba_mais"] = ""

        context["index_page"] = True
        return context


class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        "BlogPost",
        related_name="tagged_items",
        on_delete=models.CASCADE,
    )


class BlogPost(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    body = RichTextField(blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        on_delete=models.SET_NULL,
        related_name="+",
        blank=True,
        null=True,
    )
    caption = models.CharField(blank=True, max_length=250)
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)
    show_in_featured = models.BooleanField(
        default=False, verbose_name="Exibir nos cards principais"
    )

    search_fields = Page.search_fields + [
        index.SearchField("intro"),
        index.SearchField("body"),
    ]

    content_panels = Page.content_panels + [
        FieldPanel("date"),
        FieldPanel("intro"),
        FieldPanel("body", classname="full"),
        FieldPanel("image"),
        FieldPanel("caption"),
        FieldPanel("tags"),
        FieldPanel("show_in_featured"),
    ]

    def get_context(self, request):
        from home.context_processors import get_current_date_with_words

        context = super().get_context(request)

        related_posts = (
            BlogPost.objects.live()
            .public()
            .exclude(id=self.id)
            .filter(tags__in=self.tags.all())
        )

        context["related_posts"] = related_posts[:3]
        context["date"] = get_current_date_with_words(self.date)
        return context


class CarouselBlogSection(Page):
    content_panels = Page.content_panels + [
        InlinePanel("carousel_blog_slides", label="Slides"),
    ]

    parent_page_types = ["blog.BlogIndexPage"]


class CarouselBlogSlide(Orderable):
    page = ParentalKey(
        "CarouselBlogSection",
        related_name="carousel_blog_slides",
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=512, null=True, blank=True)
    description = models.CharField(max_length=512, null=True, blank=True)
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
    )
    read_more_redirect_uri = models.CharField(max_length=255, null=True, blank=True)

    panels = [
        FieldPanel("title"),
        FieldPanel("description"),
        FieldPanel("image"),
        FieldPanel("read_more_redirect_uri"),
    ]
