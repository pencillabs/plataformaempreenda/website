from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    @classmethod
    def get_or_create_user(cls, user_data):
        username = user_data["preferred_username"]
        email = user_data["email"]
        user, created = cls.objects.get_or_create(
            username=username, defaults={"email": email}
        )

        if created:
            user.first_name = user_data.get("given_name", "")
            user.last_name = user_data.get("family_name", "")
            user.save()

        return user
