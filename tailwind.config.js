/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
      './empreenda/empreenda/templates/**/*.html',
      './empreenda/home/templates/**/*.html',
      './empreenda/blog/templates/**/*.html',
      './node_modules/flowbite/**/*.js'
  ],
  theme: {
    extend: {
      colors: {
        lightYellow: '#FDD015',
        darkYellow: '#BF9E16',
        gray: '#6B7280',
        lightGray: '#E5E7EB',
        darkGray: '#393A39',
        purpleAccent: '#8B3B8F',
        blueAccent: '#3B4798',
        darkOrange: '#F4991D',
      },
      fontFamily: {
        sans: ['Poppins', 'sans-serif'], // Default 'sans' font family as Poppins
      },
    },
  },
  plugins: [
    require('flowbite/plugin')
  ],
}
